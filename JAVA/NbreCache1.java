import java.util.*;
import java.util.Random;
 
public class NbreCache1{
    public static void main(String[] args){    
        double Nombre,essais=0;
        Random rand = new Random();
        double NombreMystere = (double)rand.nextInt(101);
        System.out.println("Jeu du Nombre Cache");
        System.out.println("Trouvez le nombre mystère entre 0 et 100 !");
        do {
            Scanner clavier = new Scanner(System.in);
            System.out.println("Veuillez entrer un nombre...");
            Nombre = clavier.nextDouble();       
            if (Nombre<NombreMystere){
                System.out.println("C'est plus ! ");
                essais++;
            }
            else if (Nombre >NombreMystere){
                System.out.println("C'est moins !");
                essais++;
            }
            else{
                System.out.println("Trouver en " + (int)essais + " essais ! Bien Joué !");
            }
        }
        while (Nombre!=NombreMystere || (int)essais>10);         
    }
}