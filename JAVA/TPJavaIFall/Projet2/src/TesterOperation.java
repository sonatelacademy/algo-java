import java.util.Scanner;
public class TesterOperation {
	public static void main(String[] args) {
		try {
			Scanner clavier=new Scanner(System.in);
			int nbre1,nbre2;
			System.out.println("Entrer le premier nombre");
			nbre1=clavier.nextInt();
			System.out.println("Entrer le second nombre");
			nbre2=clavier.nextInt();
			Operation O1 = new Operation(nbre1,nbre2);
			O1.afficher();
		}
		catch (Exception e) {
			System.out.println("Impossible de faire l'Opération vous devez entrer des ENTIERS");
		}
	}
}
