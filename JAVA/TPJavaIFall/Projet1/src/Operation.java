import java.util.*;
public class Operation {
	int nbre1,nbre2;
	public Operation(int n,int d) {
		nbre1=n;
		nbre2=d;
	}
	public void afficher() {
		try {
			System.out.println(nbre1+"+"+nbre2+"="+(nbre1+nbre2));
			System.out.println(nbre1+"-"+nbre2+"="+(nbre1-nbre2));
			System.out.println(nbre1+"*"+nbre2+"="+(nbre1*nbre2));
			if(nbre2!=0) {
				System.out.println(nbre1+"/"+nbre2+"="+((float)nbre1/nbre2));
				System.out.println(nbre1+"DIV"+nbre2+"="+(nbre1/nbre2));
				System.out.println(nbre1+"%"+nbre2+"="+(nbre1%nbre2));
			}
			else {
				System.out.println("Impossible de faire la division par 0");
				System.out.println("Impossible de faire le modulo par 0");
			}
			System.out.println(nbre1+"exp"+nbre2+"="+(Math.pow(nbre1,nbre2)));
		}
		catch (Exception e) {
			System.out.println("Impossible de faire l'Opération vous devez entrer des ENTIERS");
		}
	}
}
