public class CompteCheque extends CompteBancaire {
	double decouvertAutorise;
	CompteCheque(double solde,double decouvertAutorise) {
		super(solde);
		this.decouvertAutorise = decouvertAutorise;
	}
	void retirer(double montant) {
		if(solde + decouvertAutorise>=montant)
			solde-=montant;
	}
}
