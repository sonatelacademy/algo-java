public class Personne {
	String nom;
	String prenom;
	String adresse;
	public Personne() {
		this.nom="GUEYE";
		this.prenom="Moimeme";
		this.adresse="101 Hamo 4 Guédiawaye";
	}
	public Personne(String nom, String prenom, String adresse) {
		this.nom=nom;
		this.prenom=prenom;
		this.adresse=adresse;
	}
	void afficher() {
	System.out.println("Nom: "+this.nom+", Prénom: "+this.prenom+", Adresse: "+this.adresse);
	}
}
