public class CompteBancaire {
	double solde;
	CompteBancaire(double solde) {
		this.solde=solde;
	}
	void déposer(double montant) {
		solde+=montant;
	}
	void retirer(double montant) {
		if(this.solde>=montant)
			solde-=montant;
	}
}
