/*
@author: Sadikh;
*/
public class OperationSubstract implements Strategy{
   // classe OperationSubstract qui implémente l'interface Strategy
   public int doOperation(int num1, int num2) {
   	//opération de soustraction
      return num1 - num2;
   }
}
