public class Etudiant {
	private String nom;
	private String prenom;
	private int age;
	private String mail;
	private String mobile;

	Etudiant(EtudiantMonteur monteur) {
		this.nom = monteur.nom;
		this.prenom = monteur.prenom;
		this.age = monteur.age;
		this.mail = monteur.mail;
		this.mobile = monteur.mobile;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMobile() {
		return mail;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public static class EtudiantMonteur {
		private String nom;
		private String prenom;
		private int age;
		private String mail;
		private String mobile;		
		
		public EtudiantMonteur(String nom, String prenom) {
			super();
			this.nom = nom;
			this.prenom = prenom;
		}
		public EtudiantMonteur age(int age) {
			this.age = age;
			return this;
		}
		public EtudiantMonteur mail(String mail) {
			this.mail = mail;
			return this;
		}
		public EtudiantMonteur mobile(String mobile) {
			this.mobile = mobile;
			return this;
		}
		public Etudiant monter() {
			return new Etudiant(this);
		}
	}
}
