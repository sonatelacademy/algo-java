/*
@author: Sadikh;
*/
public class OperationAdd implements Strategy{
   	//classe OpérationAdd qui implémente l'interface Startégy
   public int doOperation(int num1, int num2) {
      // l'opération d'addition
      return num1 + num2;
   }
}
