public class DemonPatternMonteur {
	public static void main(String[] args) {
		Etudiant etudiant = new Etudiant.EtudiantMonteur("GUEYE", "Ababacar Sadikh").mail("abougueye96@yahoo.fr").monter();
		System.out.println(etudiant.getPrenom());
		System.out.println(etudiant.getNom());
		System.out.println(etudiant.getMail());
	}
}
