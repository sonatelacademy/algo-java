/*
@author: Sadikh;
*/
public class OperationMultiply implements Strategy{
	//classe OpérationMultiply qui implémente l'interface Startégy
   public int doOperation(int num1, int num2) {
   	// opération multlication avec deux paramètres
      return num1 * num2;
   }
}
