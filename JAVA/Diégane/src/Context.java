/*
@author: Sadikh;
*/
public class Context {
	//délaration d'une instance privée de Strategy
   private Strategy strategy;

   public Context(Strategy strategy){
   	//constructeur
      this.strategy = strategy;
   }
   //exécution d'opération
   public int executeStrategy(int num1, int num2){
      return strategy.doOperation(num1, num2);
   }
}
