/*
@author: Sadikh;
*/
public interface Strategy {
	//interface Strategy avec sa methode doOperation avec deux paramètres
   public int doOperation(int num1, int num2);
}
