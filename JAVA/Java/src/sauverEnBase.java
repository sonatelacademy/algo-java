public static void sauverEnBase(String personne) {
	// Information d'accès à la base de données
	String url = "jdbc:mysql://localhost/personne";
	String login = "root";
	String password = "";
	Connection cn = null;
	Statement st = null;
	try {
		// Etape 1: Chargement du driver
		Class.forName("com.mysql.jdbc.Driver");
		// Etape 2: récupération de la connexion
		cn = DriverManager.getConnection(url,login,password);
		// Etape 3: Création d'un statement
		st = cn.createStatement();
		String sql = "INSERT INTO 'tour' ('nom') VALUES ('"+personne+"')";
		// Etape 4: Exécution de la requête
		st.executeUpdate(sql);
	} catch (SQLException e) {
		System.out.println("Connexion refusé ou Base inconnue");
	} catch (ClassNotFoundException e) {
		System.out.println("Driver non trouvé");
	} finally {
		try {
			// Etape 5: Libération des ressources de la mémoire
			cn.close();
			st.close();
		} catch (SQLException e) {
			System.out.println("Connexion refusée ou Base inconnue");
		}
	}
}
