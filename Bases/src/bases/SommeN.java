package bases;
import java.util.Scanner;
public class SommeN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		char reponse = ' ';
		do {
			
			int som =0, i=1, n;
			
			System.out.println("Bienvenu! ce programme fait la somme de 1 jusqu'à N avec N une valeur entrée par l'utilisateur");
			System.out.println("Veuillez entrer la valeur de N");
			n = clavier.nextInt();
			while(i<n+1) {
				som+=i;
				i++;
			}
			System.out.println("La Somme des valeurs de 1 à "+n+" est: "+som);
			
			do{
			    System.out.println("Voulez-vous tester un autre Somme N ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
	}

}
