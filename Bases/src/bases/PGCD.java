package bases;
import java.util.Scanner;
public class PGCD {

	  public static void main(String[] args) {
		  Scanner clavier = new Scanner(System.in);
		  System.out.println("Bienvenu sur le menu PGCD");
		  char reponse =' ';
		  do {
			  System.out.println("Entrer le 1er nombre");
			  int a = clavier.nextInt();
			  System.out.println("Entrer le 2eme nombre");
			  int b = clavier.nextInt();
			  if ( a > 0 && b > 0 ) {
			      System.out.print("PGCD("+a+","+b+") = ");
			      while ( a != b ) {
					if ( a < b )
					  b = b - a;
					else
					  a = a - b;
					System.out.print("PGCD("+a+","+b+") = ");
				  }
				  System.out.println(a);
			 }
			  do{
				    System.out.println("Voulez-vous tester un autre PGCD ? (O/N)");
				    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		  }while(reponse=='0' || reponse=='o');
	  }
}
