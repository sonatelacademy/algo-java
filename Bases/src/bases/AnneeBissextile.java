package bases;
import java.util.Scanner;
import java.text.*;
public class AnneeBissextile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue dans ce plateforme");
		System.out.println("Vous voulez savoir si l'année est bissextile?");
		char reponse = ' ';
		do {
			System.out.println("Entrez une année: ");
			int annee = clavier.nextInt();
			if(annee%4==0 && annee%100!=0) {
				System.out.println("L'année "+annee+" est bien bissextile");
			}
			else if(annee%400==0) {
				System.out.println("L'année "+annee+" est bien bissextile");
			}
			else {
				System.out.println("L'année "+annee+" n'est pas bissextile");
			}
			do{
			    System.out.println("Voulez-vous voir pour une autre année ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
		System.out.println("Merci!");
	}
}
