package bases;
import java.util.Scanner;
public class Occurence1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme nous donne le premier et dernier occurrence d'un nombre dans un table");
		char reponse = ' ';
		do {
			int tab, pos=0, oc1=0;
			int prem=0,prem1=0;
			System.out.println("Veuillez entrer l'occurence que tu cherche");
			int nbre = clavier.nextInt();
			System.out.println("Voici Comment ça marche:");
			System.out.println("Entrez des valeurs et tapez 0 si vous voulez arreter.");
			System.out.println("Entrez les valeurs ");
			do {
				tab = clavier.nextInt();
				oc1++;
				if(tab==nbre) {
					pos++;
					if(pos==1) {
						prem = oc1;
						prem1 =oc1;
					}
					
					else {
						if(pos>1) {
							prem1 = oc1;
						}
					}
				}
				
			}while(tab!=0);
			if(prem!=0) {
				System.out.println("Le premier occurrence de "+nbre+" se trouve en position "+prem);
				System.out.println("Le dernier occurrence de "+nbre+" se trouve en position "+prem1);
			}
			else {
				System.out.println("Il n'existe pas d'occurence dans ce tableau");
				System.out.println("Donc la position c'est 0");
			}
			do{
			    System.out.println("Voulez-vous tester un autre occurrence ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse=='O' || reponse=='o');
		System.out.println("Merci pour l'utilisation");
	}

}
