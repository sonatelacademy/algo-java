package bases;
import java.util.Scanner;
public class SuiteAlterne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		char reponse = ' ';
		System.out.println("Bienvenue! Ce programme fait une suite alterné des nombres compris entre 1 et N");
		do {	
			System.out.println("Veuillez entrer le nombre dont vous cherchez ces alternances");
			int nbre = clavier.nextInt();
			System.out.print("1 ");
			for(int i=2;i<=nbre;i++) {
				if(i%2==0) {
					int fact=1;
					for(int j=1;j<=i;j++) {
						fact =(fact*j);
					}
					System.out.print("-"+fact+" ");
				}
				else {
					int fact1=1;
					for(int j=1;j<=i;j++) {
						fact1 =fact1*j;
					}
					System.out.print(fact1+" ");
				}
			}
			do{
			    System.out.println("Voulez-vous tester une autre Suite Alterné ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
		System.out.println("Merci pour l'utilisation");
	}

}
