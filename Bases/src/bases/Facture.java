package bases;
import java.util.Scanner;
public class Facture {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme fait le total d'une facture");
		char reponse = ' ';
		do {
			int quantité = 0;
			int prix = 0;
			int total = 0;
			System.out.println("Pour la saisie on s'arrête quand tu tape 0 comme quantité");
			do {
				System.out.println("Entrer la quantité");
				quantité = clavier.nextInt();
				if(quantité!=0) {
					System.out.println("Entrer le prix");
					prix = clavier.nextInt();
				}
				total = total+(quantité*prix);
			}while(quantité!=0);
			System.out.println("Le total de la facture est égale à "+total);
			do{
			    System.out.println("Voulez-vous faire une autre facture ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse=='O' || reponse=='o');
		System.out.println("Merci pour l'utilisation de ce programme");
		}
}
