package bases;
import java.util.Scanner;
public class SommePI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		char reponse = ' ';
		do {
			int somp=0, somi=0, i = 1, n = 0;
			System.out.println("Bienvenu! Ce programme fait la somme des nombres paires à part et des nombres impaires de N valeur saisie par l'utilisateur");
			System.out.println("Vous voulez taper combien de valeurs?");
			n = clavier.nextInt();
			while(i<=n) {
				System.out.println("Entrez la valeur n°"+i);
				int x = clavier.nextInt();
				if(x%2==0) {
					somp+=x;
				}
				else {
					somi+=x;
				}
				i++;
			}
			if(somp==0) {
				System.out.println("Vous n'avez pas saisie de nombres paires");
			}
			else {
				System.out.println("La somme des nombres paires entrées vaut: "+somp);
			}
			if(somi==0) {
				System.out.println("Vous n'avez pas saisie de nombres paires");
			}
			else {
				System.out.println("La somme des nombres impaires entrées vaut: "+somi);
			}
			
			do{
			    System.out.println("Voulez-vous tester une autre Somme paire /impaire ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
	}

}
