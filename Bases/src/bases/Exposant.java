package bases;
import java.util.Scanner;
public class Exposant {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenu! ce programme calcule l'exposant de X à la puissance n");
		char reponse = ' ';
		do {	
			System.out.println("Voici le format XexpN");
			System.out.println("Veuillez entrer X");
			int x = clavier.nextInt();
			System.out.println("Veuillez entrer N");
			int n = clavier.nextInt();
			int i=0, exp=1;
			if(n==0) {
				System.out.println(x+" exposant "+n+" est égale à 1");
			}
			else {
				while(i<n) {
					exp=exp*x;
					i++;
				}
				System.out.println(x+" exp "+n+" est = "+exp);
			}
			
			do{
			    System.out.println("Voulez-vous tester un autre exposant ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
		
	}

}
