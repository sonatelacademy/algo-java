package bases;
import java.util.Scanner;
public class Triangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner clavier =  new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme affiche un triangle en chiffre");
		char reponse = ' ';
		do {
			System.out.println("Voulez voulez afficher combier de chiffre?");
			int a = clavier.nextInt();
			for(int i=1;i<=a;i++) {
				for(int j=1;j<=i;j++) {
					System.out.print(j);
				}
				System.out.println();
			}
			do{
			    System.out.println("Voulez-vous tester pour un autre nombre ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o'); 
		System.out.println("Merci pour l'utilisation");
	}

}
