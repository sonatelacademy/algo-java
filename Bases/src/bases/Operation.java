package bases;
import java.util.Scanner;
public class Operation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue sur le menu des Opérations");
		char reponse = ' ';
		do {
			System.out.println("Enter le premier nombre");
			double a = clavier.nextDouble();
			System.out.println("Enter le deuxième nombre");
			double b = clavier.nextDouble();
			System.out.println("Voici le menu des opérations");
			System.out.println("Tapez * pour faire la Multiplication");
			System.out.println("Tapez + pour faire la Somme");
			System.out.println("Tapez - pour faire la Soustraction");
			System.out.println("Tapez / pour faire la division");
			System.out.println("Tapez % pour faire le modulo");
			System.out.println("Enter l'opérateur que tu veux parmis ces derniers");
			String choix = clavier.next();
			
			switch(choix) {
				case "*" : System.out.println("La Multiplication de "+a+" et de "+b+ " est "+(a*b));
				case "+" : System.out.println("La Somme de "+a+" et de "+b+ " est "+(a+b));
				case "-" : System.out.println("La Soustraction de "+a+" et de "+b+ " est "+(a-b));
				case "/" : if(b!=0) {
					System.out.println("La Division de "+a+" et de "+b+ " est "+(a/b));
				}
				else {
					System.out.println("Division Par 0 impossible de faire l'opération à notre niveau");
				}
				case "%" : if(b!=0) {
					System.out.println("Le Modulo de "+a+" et de "+b+ " est "+(a%b));
				}
				else {
					System.out.println("Division Par 0 impossible de faire l'opération à notre niveau");
				}
				default : System.out.println("Opération inconnue");
			}
			
			do{
			    System.out.println("Voulez-vous tester un autre opération ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
	}
}
