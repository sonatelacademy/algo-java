package bases;
import java.util.Scanner;

public class Somme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char reponse = ' ';
		Scanner clavier = new Scanner(System.in);
		do {
			String param1 = args[0];
			String param2 = args[1];
			
			if(args.length == 2) {
				System.out.println("Bonjour "+param1+" "+param2+" Bienvenu dans l'application Somme:");
				System.out.println("Saisissez la 1ere valeur");
				int a = clavier.nextInt();
				System.out.println("Saisissez la 2eme valeur");
				int b = clavier.nextInt();
				int som = a+b;
				System.out.println("La somme de "+a+" et de "+b+" est "+som);
			}
			else {
				System.out.println("Les arguments entrés sont incorrectes");
			}
			
			do{
			    System.out.println("Voulez-vous tester une autre Somme ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
	}

}
