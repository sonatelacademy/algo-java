package bases;
import java.util.Scanner;
public class BoucleDoWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String prenom = new String();
		//Pas besoin d'initialiser : on entre au moins une fois dans la boucle !
		char reponse = ' ';
		 
		Scanner clavier = new Scanner(System.in);
		 
		do{
		  System.out.println("Donnez un prénom : ");
		  prenom = clavier.nextLine();
		  System.out.println("Bonjour " +prenom+ ", comment vas-tu ?");
		       
		  do{
		    System.out.println("Voulez-vous réessayer ? (O/N)");
		    reponse = clavier.nextLine().charAt(0);
		  }while(reponse != 'O' && reponse != 'N');
		        
		}while (reponse == 'O');
		 
		System.out.println("Au revoir…");
		
	}

}
