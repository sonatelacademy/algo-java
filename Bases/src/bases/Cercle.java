package bases;
import java.util.Scanner; 
import java.math.*;
@SuppressWarnings("unused")
public class Cercle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		char reponse = ' ';
		System.out.println("Bienvenu! Ce programme calcule la Surface et le périmètre d'un Cercle");
		do {	
			System.out.println("Veuillez entrer le rayon");
			double r = clavier.nextDouble();
			double p = 2*r*Math.PI;
			double s = Math.PI*r*r;
			System.out.println("Le Périmétre d'un cercle se calcule comme suit:");
			System.out.println("Perimétre = 2piR");
			System.out.println("Donc le périmétre d'un cercle de rayon R="+r+" est :"+p);
			System.out.println("La Surface d'un cercle se calcule comme suit:");
			System.out.println("Surface = piRR");
			System.out.println("Donc la Surface d'un cercle de rayon R="+r+" est :"+s);
			do{
			    System.out.println("Voulez-vous tester pour un autre cercle ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse == 'o');
	}
}
