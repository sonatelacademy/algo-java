package bases;
import java.util.Scanner;
public class PPCM {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule le PPCM de 2 nombres");
		char reponse = ' ';
		do {	
			System.out.println("Entrez la Première valeur");
			int x = clavier.nextInt();
			System.out.println("Entrez la Deuxième valeur");
			int y = clavier.nextInt();
			int pp = 0;
			if(x==y) {
				System.out.println("Le PPCM de "+x+" et de "+y+" est égale à "+x);
			}
			else {
				if(x<y) {
					pp=y+1;
				}
				else {
					pp=x+1;
				}
				while(pp%x !=0 || pp%y !=0) {
					pp++;
				}
				System.out.println("Le PPCM de "+x+" et de "+y+" est égale à "+pp);
			}
			do{
			    System.out.println("Voulez-vous voir pour une autre PPCM ? (O/N)");
			    reponse = clavier.next().charAt(0);
			  }while(reponse != 'O' && reponse != 'o' && reponse != 'n' && reponse != 'N');
		}while(reponse == 'O' || reponse =='o');
		System.out.println("Merci!");
	}
}