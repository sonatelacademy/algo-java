package Algo;
import java.util.*;
public class PositifNegatifTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme affiche le nombres de valeurs négatives et positives d'un tableau saisie par l'utilisateur");
		System.out.println("Vous Voulez entrer combien de valeurs? En d'autres terme, la taille du tableau?");
		int n = clavier.nextInt();
		int tab[]=new int[n];
		int tabn[]=new int[n];
		int tabp[]=new int[n];
		int nbrep=0,nbre=0,nbren=0,j=0,k=0;
		for (int i = 0; i < tab.length; i++) {
			System.out.println("Valeur "+(i+1)+"?");
			tab[i] = clavier.nextInt();
			if(tab[i]>0) {
				tabp[j]=tab[i];
				nbrep++;
				j++;
			}
			else if(tab[i]<0) {
				tabn[k]=tab[i];
				nbren++;
				k++;
			}
		}
		System.out.println("Le nombre de valeurs positives dans ce tableau est "+nbrep);
		System.out.println("Ce sont: ");
		for (int i = 0; i < tabp.length; i++) {
			if(tabp[i]!=0) {
				System.out.print(tabp[i]+" ");
			}
		}
		System.out.println();
		System.out.println("Le nombre de valeurs négatives dans ce tableau est "+nbren);
		System.out.println("Ce sont: ");
		for (int i = 0; i < tabn.length; i++) {
			if(tabn[i]!=0) {
				System.out.print(tabn[i]+" ");
			}
		}
	}

}
