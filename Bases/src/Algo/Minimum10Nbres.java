package Algo;

import java.util.Scanner;

public class Minimum10Nbres {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme cherche le minimum de 10 nombres");
		int pos=0, min = 0;
		for(int i=1;i<=10;i++) {
			System.out.println("Entrez le nombre numéro "+i);
			int n = clavier.nextInt();
			if(i==1) {
				min=n;
			}else if(min>n) {
				min=n;
				pos=i;
			}
		}
		System.out.println("Le minimum des 10 nombres est "+min+" et se trouve à la "+pos+"ième position");
	}

}
