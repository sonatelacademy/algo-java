package Algo;
import java.util.Scanner;
public class PairImpairTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule la sommes des nombres paires et impaires d'un tableau d'entiers définie");
		int tab[]= {12,15,17,38,32,27,11,19,23,40,92,56,77,85,66,48,97,78,8};
		System.out.println();
		System.out.println("Les nombres Paires du tableau sont: ");
		int som1=0;
		for (int i = 0; i < tab.length; i++) {
			if(tab[i]%2==0) {
				if(tab[i]!=tab.length) {
					System.out.print(tab[i]+"  ");
				}
				else {
					System.out.print(tab[i]+" ");
				}
					som1+=tab[i];
					
			}
		}
		System.out.println();
		System.out.println("La somme des nombre paires est : "+som1);
		System.out.println();
		System.out.println("Les nombres Impaires du tableau sont: ");
		int som=0;
		for (int i = 0; i < tab.length; i++) {
			if(tab[i]%2!=0) {
				if(tab[i]!=tab.length) {
					System.out.print(tab[i]+"  ");
				}
				else {
					System.out.print(tab[i]+" ");
				}
				
				som+=tab[i];
			}
		}
		System.out.println();
		System.out.println("La somme des nombre impaires est : "+som);
	}

}
