package Algo;
import java.util.*;
public class SaisieTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		int tab[] = new int[9];
		System.out.println("Bienvenue! Ce programme demande à l'utilisateur d'entrer les valeurs d'un tableau de 9 éléments.");
		for (int i = 0; i < tab.length; i++) {
			System.out.println("Saisissez la valeur n° "+(i+1));
			tab[i]=clavier.nextInt();
		}
		System.out.println("Les valeurs saisies sont: ");
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
		}
	}

}
