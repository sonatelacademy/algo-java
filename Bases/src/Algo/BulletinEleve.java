package Algo;
import java.util.Scanner;
public class BulletinEleve {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule la note des élèves, leurs moyennes et une moyenne classe");
		int eleves=1;
		double moy=0,moy1=0,moyg=0, moyclass=0;
		do {
			System.out.println("Notes de l'éleves n° "+eleves);
			double som=0, nbre=0, note;
			do {
				do {
					note =clavier.nextInt();
					if(note>20 || note<0 && note!=-1) {
						System.out.println("Impossible d'entrer une note négative ou supérieure à 20");
					}
				} while ((note>20 || note<0) && note!=-1);
				if(note!=-1 && (note<=20 && note>=0)) {
					nbre++;
					som+=note;
				}
			} while (note!=-1);
			moy=som/nbre;
			moy1=(double)((int)(moy*100))/100;
			System.out.println("Moyenne Élèves n° "+eleves+" : "+moy1);
			moyg+=moy1;
			eleves++;
		} while (eleves<=25);
		moyclass=moyg/25;
		moyclass=(double)((int)(moyclass*100))/100;
		System.out.println("La Moyenne de La classe est "+moyclass);
	}

}
