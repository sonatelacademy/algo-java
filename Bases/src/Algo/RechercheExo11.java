package Algo;
import java.util.Scanner;
public class RechercheExo11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme fait une opération de 3 entiers choisit par l'utilisateur");
		System.out.println("Entrer le premier entier");
		int nbre1 = clavier.nextInt();
		System.out.println("Entrer le deuxième entier");
		int nbre2 = clavier.nextInt();
		System.out.println("Entrer le troisième entier");
		int nbre3 = clavier.nextInt();
		System.out.println("**********************MENU**********************");
		System.out.println("1 ********************SOMME*********************");
		System.out.println("2 ********************PRODUIT*******************");
		System.out.println("3 ********************MOYENNE*******************");
		System.out.println("4 ********************MINIMUM*******************");
		System.out.println("5 ********************MAXIMUM*******************");
		System.out.println();
		System.out.println("Veuillez Saisir l'opération");
		int operation = clavier.nextInt();
		switch (operation) {
		case 1:
			System.out.println(nbre1+" + "+nbre2+" + "+nbre3+" = "+(nbre1+nbre2+nbre3));
			break;
		case 2:
			System.out.println(nbre1+" x "+nbre2+" x "+nbre3+" = "+(nbre1*nbre2*nbre3));
			break;
		case 3:
			System.out.println(nbre1+" + "+nbre2+" + "+nbre3+" = "+((nbre1+nbre2+nbre3)/3.0));
			break;
		case 4:
			if(nbre1<=nbre2 && nbre2<=nbre3) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre1);
			}else if(nbre2<=nbre1 && nbre1<=nbre3) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre2);
			}else if(nbre3<=nbre1 && nbre1<=nbre2) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre3);
			}else if(nbre1<=nbre2 && nbre3<=nbre2 && nbre1<=nbre3) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre1);
			}else if(nbre2<=nbre1 && nbre3<=nbre1 && nbre2<=nbre3) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre2);
			}
			else if(nbre3<=nbre2 && nbre1<=nbre2 && nbre3<=nbre1) {
				System.out.println("Le minimum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre3);
			}
			break;
		case 5:
			if(nbre1>=nbre2 && nbre2>=nbre3) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre1);
			}else if(nbre2>=nbre1 && nbre1>=nbre3) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre2);
			}else if(nbre3>=nbre1 && nbre1>=nbre2) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre3);
			}else if(nbre1>=nbre2 && nbre3>=nbre2 && nbre1>=nbre3) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre1);
			}else if(nbre2>=nbre1 && nbre3>=nbre1 && nbre2>=nbre3) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre2);
			}
			else if(nbre3>=nbre2 && nbre1>=nbre2 && nbre3>=nbre1) {
				System.out.println("Le maximum entre "+nbre1+" , "+nbre2+" et "+nbre3+" est "+nbre3);
			}
			break;
		default:
			System.out.println("Opération Non Réconnue");
			break;
		}
	}

}
