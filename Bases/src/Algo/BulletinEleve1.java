package Algo;
import java.util.Scanner;
public class BulletinEleve1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule la note des élèves, leurs moyennes et une moyenne classe");
		int eleves=1;
		double tab[] = new double[25];
		double moy=0,moy1=0,moyg=0, moyclass=0;
		do {
			System.out.println("Notes de l'éleves n° "+eleves);
			double som=0, nbre=0, note;
			do {
				do {
					note =clavier.nextInt();
					if(note>20 || note<0 && note!=-1) {
						System.out.println("Impossible d'entrer une note négative ou supérieure à 20");
					}
				} while ((note>20 || note<0) && note!=-1);
				if(note!=-1 && (note<=20 && note>=0)) {
					nbre++;
					som+=note;
				}
			} while (note!=-1);
			moy=som/nbre;
			moy1=(double)((int)(moy*100))/100;
			tab[eleves-1]=moy1;
			eleves++;
		} while (eleves<=tab.length);
		
		for (int i = 0; i < tab.length; i++) {
			System.out.println("Moyenne de l'élèves n° "+(i+1)+" : "+tab[i]);
			moyg+=tab[i];
		}
		
		moyclass=moyg/tab.length;
		moyclass=(double)((int)(moyclass*100))/100;
		System.out.println("La Moyenne de La classe est "+moyclass);
	}

}
