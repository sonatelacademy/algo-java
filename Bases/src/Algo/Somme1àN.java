package Algo;
import java.util.Scanner;
public class Somme1àN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcul la somme de 0 à N");
		System.out.println("Entrer une nombre");
		int nbre = clavier.nextInt();
		int som=0;
		for (int i = 1; i <=nbre; i++) {
			som+=i;
			if(i!=nbre) {
				System.out.print(i+" + ");
			}
			else {
				System.out.print(i+" = ");
			}
		}
		System.out.println(som);
	}

}
