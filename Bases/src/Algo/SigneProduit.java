package Algo;
import java.util.Scanner;
public class SigneProduit {

	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme nous dit si le produit de deux variables est positif ou négatif");
		System.out.println("Saisir la première valeur");
		int nbre1 = clavier.nextInt();
		System.out.println("Saisir la seconde valeur");
		int nbre2 = clavier.nextInt();
		if((nbre1>0 && nbre2>0) || (nbre1<0 && nbre2<0)) {
			System.out.println("Le produit de "+nbre1+" et de "+nbre2+" est positif");
		}
		else if((nbre1>0 && nbre2<0) || (nbre2>0 && nbre1<0)) {
				System.out.println("Le produit de "+nbre1+" et de "+nbre2+" est négatif");

		}
		else {
			System.out.println("Le produit de "+nbre1+" et de "+nbre2+" est égal à 0");
		}
	}

}
