package Algo;
import java.util.*;
public class MinMaxTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int tab[]= {12,15,17,38,32,27,11,19,23,40,92,56,77,85,66,48,97,78,8};
		int tab1 [] = new int[tab.length];
		System.out.println("Bienvenue! Ce programme calcule le minimum et le maximum d'un tableau d'entier définis.");
		System.out.println();
		System.out.println("Avant Tri: ");
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
		}
		System.out.println();
		System.out.println("Après Tri: ");
		for (int i = 0; i < tab.length; i++) {
			for(int j = i; j < tab.length; j++) {
				if(tab[j]<tab[i]) {
					tab1[i]=tab[i];
					tab[i]=tab[j];
					tab[j]=tab1[i];
				}
			}
			System.out.print(tab[i]+" ");
		}
		System.out.println();
		System.out.println("Le Minimum est "+tab[0]);
		System.out.println("Le Maximum est "+tab[tab.length-1]);
	}

}
