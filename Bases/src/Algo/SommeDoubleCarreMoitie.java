package Algo;
import java.util.Scanner;
public class SommeDoubleCarreMoitie {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcul la somme de N valeur puis donne la moitié du carré du double de la somme.");
		System.out.println("Il faut saisir des nombres et pour arrêter, tapez -1");
		double nbre, som=0, carré, res=0;
		do {
			System.out.println("Saisir un nombre");
			nbre = clavier.nextInt();
			if(nbre!=-1) {
				som+=nbre;
				carré=((2*som)*(2*som));
				res = carré/2;
			}
		}while(nbre != -1);
		System.out.println("La somme des N valeurs entrées est :"+som);
		System.out.println("La Moitié du Carré du Double de cette somme est: "+res);
	}

}
