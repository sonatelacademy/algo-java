package Algo;
public class Permutation {

	public static void main(String[] args) {
		System.out.println("Bienvenue! Ce programme fait une permutation de 3 variables");
		int a=5,b=9,c=20;
		System.out.println("On transfert A vers B, B vers C et C vers A");
		System.out.println("On a au départ A="+a+" B="+b+" et C="+c);
		int D;
		D=a;
		a=b;
		b=c;
		c=D;
		System.out.println("Maintenant on a A="+a+" B="+b+" et C="+c);
	}
}
