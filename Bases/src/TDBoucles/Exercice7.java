package TDBoucles;
import java.util.Scanner;
public class Exercice7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcul tes chances de gagner au tiercé, quarté, quinté et autres impôts volontaires.");
		System.out.println("Entrez le nombre de chevaux partants");
		int n = clavier.nextInt();
		System.out.println("Entrez le nombre de chevaux joués");
		int p = clavier.nextInt();
		System.out.println("La formule pour déterminer X est: X=n!/(n-p)!");
		System.out.println("La formule pour déterminer Y est: Y=n!/(p!*(n-p)!)");
		System.out.println("Avec n le nombre de chevaux partants et p le nombre de cheveaux joués");
		int nfact=1, pfact=1, npfact=1;
		for(int i=1;i<=n;i++) {
			nfact=nfact*i;
		}
		for(int i=1;i<=p;i++) {
			pfact = pfact*i;
		}
		for(int i=1;i<=(n-p);i++) {
			npfact=npfact*i;
		}
		double X = (double)nfact/npfact;
		double Y = (double)nfact/(pfact*npfact);
		
		System.out.println("Dans L'orde: Vous avez 1 sur "+X+" chance de gagner");
		System.out.println("Dans le Désordre: Vous avez 1 sur "+Y+" chance de gagner");
	}
}
