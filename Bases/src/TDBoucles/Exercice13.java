package TDBoucles;
import java.util.Scanner;
public class Exercice13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule la somme dres fraction de 1/n");
		System.out.println("Saisissez la valeur de n");
		double n = clavier.nextDouble();
		Double som=1.0;
		System.out.print("1  ");
		for(int i =2; i<=n; i++) {
			som = som+(1.0/(double)i);
			System.out.print("1/"+i+"  ");
		}
		System.out.println();
		System.out.println("La somme des nombres de 1 à "+n+" est 1/"+n+" = "+som);
	}

}
