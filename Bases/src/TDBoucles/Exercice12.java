package TDBoucles;
import java.util.Scanner;
public class Exercice12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule la hauteur annelle des plus");
		int hauteur;
		int max=0, min=0,moy=0;
		int posmax=0,posmin=0;
		for(int j=1;j<=12;j++) {
			System.out.println("Entrez la hauteur de la plus du mois n°"+j);
			hauteur =clavier.nextInt();
			moy=moy+hauteur;
			if(max<hauteur) {
				max=hauteur;
				posmax=j;
				if(min>hauteur) {
					min=hauteur;
					posmin=j;
				}
				else {
					min=min;
					posmin=j;
				}
			}
			else {
				if(min>max) {
					min=max;
					posmin=j;
				}
			}
		}
		System.out.println("La hauteur de pluie la plus forte est "+max+" c'est au mois n°"+posmax);
		System.out.println("La hauteur de pluie la plus faible est "+min+" c'est au mois n°"+posmin);
		System.out.println("La moyenne des hauteurs de pluies tombés est :"+(moy/12.0));
	}
}
