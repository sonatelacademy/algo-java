package TDBoucles;
import java.util.Scanner;
public class Exercice16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme demande un nombre N et affiche le plus petit nombre dont la puissance à 2 est supérieure à N");
		System.out.println("Entrer une valeur");
		int n = clavier.nextInt();
		int nbre = 1;
		do {
			nbre = nbre*2;
			
		}while(n>=nbre);
		System.out.println("le plus petit exposant de 2 qui est supérieur à "+n+" est "+nbre);

	}

}