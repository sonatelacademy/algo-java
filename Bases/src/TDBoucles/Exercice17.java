package TDBoucles;
import java.util.Scanner;
public class Exercice17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcul la suite de: ");
		System.out.println("Un = 3Un-1 + 4Un-2 avec U0=4 et U1=2");
		System.out.println("Entrer la valeur de n");
		int n = clavier.nextInt();
		int U0=4, U1=2;
		int som1 = U1;
		int som2 = U0;
		int som=0; //= 3*som1+4*som2;
		for(int i =2; i<=n; i++) {
			som = 3*som1 + 4*som2;
			som2=som1;
			som1=som;
			System.out.println("U"+i+" = "+som);
		}
		
		System.out.println("La suite des termes de 0 à "+n+" est "+som);
	}

}
