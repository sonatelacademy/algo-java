package TDBoucles;
import java.util.Scanner;
public class Exercice2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme demande à l'utilisateur de Saisir un valeur compris entre 1 et 3");
		int n;
		do {
			System.out.println("Saisir une valeur:");
			n = clavier.nextInt();
			if(n>20) {
				System.out.println("plus petit please");
			}
			else if(n<10){
				System.out.println("plus grand please");
			}
			else {
				System.out.println("Bravo! Il fallait saisir un nombre compris entre 10 et 20");
			}
		}while((n>20) || (n<10));
	}

}
