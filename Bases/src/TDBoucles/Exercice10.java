package TDBoucles;
import java.util.Scanner;
public class Exercice10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme fait une suite alterné des nombres compris entre 1 et N");	
			System.out.println("Veuillez entrer le nombre dont vous cherchez ces alternances");
			int nbre = clavier.nextInt();
			System.out.print("1 ");
			for(int i=2;i<=nbre;i++) {
				if(i%2==0) {
					int fact=1;
					for(int j=1;j<=i;j++) {
						fact =(fact*j);
					}
					System.out.print("-"+fact+" ");
				}
				else {
					int fact1=1;
					for(int j=1;j<=i;j++) {
						fact1 =fact1*j;
					}
					System.out.print(fact1+" ");
				}
			}
	}

}
