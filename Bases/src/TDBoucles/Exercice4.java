package TDBoucles;
import java.util.Scanner;
public class Exercice4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme cherche le maximum de 20 nombres");
		int max = 0;
		for(int i=1;i<=20;i++) {
			System.out.println("Entrez le nombre numéro "+i);
			int n = clavier.nextInt();
			if(max<n) {
				max=n;
			}
		}
		System.out.println("Le maximum des 20 nombres est "+max);
	}

}
