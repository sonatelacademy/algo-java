package TDBoucles;
import java.util.Scanner;
public class Exercice1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme demande à l'utilisateur de Saisir un valeur compris entre 1 et 3");
		int n;
		do {
			System.out.println("Saisir une valeur:");
			n = clavier.nextInt();
			if(n<1 ||n>3) {
				System.out.println("Saisir un nombre compris entre 1 et 3");
			}
			else {
				System.out.println("excellent vous avez saisie "+n);
			}
		}while((n>3) || (n<1));
	}

}
