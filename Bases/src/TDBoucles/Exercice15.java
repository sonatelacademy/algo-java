package TDBoucles;
import java.util.Scanner;
public class Exercice15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme donne l'année ou on atteint le seuil d'une population");
		double taux = 50;
		System.out.println("Entrez l'année actuel");
		int année = clavier.nextInt();
		System.out.println("Entrez la population actuel");
		double pop = clavier.nextDouble();
		System.out.println("Entrez le seuil de la population à atteindre");
		double seuil = clavier.nextDouble();
		System.out.println("Tu défini le taux de croissance et tu laisse le programme le faire? O/N");
		char reponse = clavier.next().charAt(0);
		if(reponse =='O' || reponse == 'o') {
			System.out.println("Saisir le taux de croissance");
			taux = clavier.nextDouble();
		}
		double popfin;
		int i=0;
		do {
			popfin=pop+(taux/100*pop);
			i++;	
		}while(seuil<popfin);
		System.out.println("L'année ou vous allez atteindre votre seuil est "+(année+i));

	}

}
