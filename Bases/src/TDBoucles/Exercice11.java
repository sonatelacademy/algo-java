package TDBoucles;
import java.util.Scanner;
public class Exercice11 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme fait le total d'une facture");
		int quantité = 0;
		int prix = 0;
		int total = 0;
		System.out.println("Pour la saisie on s'arrête quand tu tape 0 comme quantité");
		do {
			System.out.println("Entrer la quantité");
			quantité = clavier.nextInt();
			if(quantité!=0) {
				System.out.println("Entrer le prix");
				prix = clavier.nextInt();
			}
			total = total+(quantité*prix);
		}while(quantité!=0);
		System.out.println("Le total de la facture est égale à "+total);
	}
}

