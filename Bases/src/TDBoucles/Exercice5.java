package TDBoucles;
import java.util.Scanner;
public class Exercice5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		int n,i=1, max=0;
		
		do {
			System.out.println("Entrer le nombre numéro "+i);
			n = clavier.nextInt();
			if(max<n) {
				max=n;
			}
			i++;
		}while(n!=0);
		System.out.println("Le nombre maximal des "+(i-2)+" nombres est "+max);
	}

}
