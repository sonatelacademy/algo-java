package Algo1;

public class DécalageTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Bienvenue! Ce programme Fait une décalage d'une position d'un tableau");
		char tab[] = {'D','E','C','A','L','A','G','E'};
		System.out.println("Avant décalage on a");
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
		}
		System.out.println();
		System.out.println("Après décalage on a");
		char oc1 = tab[0];
		for (int i = 1; i < tab.length; i++) {
			tab[i-1]=tab[i];
			if (i==(tab.length-1)) {
				tab[tab.length-1]=oc1;
			}
		}
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
		}
	}

}
