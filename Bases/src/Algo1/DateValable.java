package Algo1;
import java.util.Scanner;
public class DateValable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme vérifie si la date entrée est valable ou pas");
		System.out.println("Voici le Format JJ/MM/AAAA");
		System.out.println("Entrer le Jour J");
		int jj = clavier.nextInt();
		System.out.println("Entrer le Numéro du Mois: ");
		int mois = clavier.nextInt();
		System.out.println("Entrer l'année: ");
		int année = clavier.nextInt();
		if(année>=0 && (mois>=1 && mois <=12)) {
			if((mois==1 || mois==3 || mois==5 || mois==7 || mois==8 || mois==10 || mois==12)) {
				if((jj>=1 && jj<=31)) {
					System.out.println(jj+"/"+mois+"/"+année+" est bien valide");
				}
				else {
					System.out.println(jj+"/"+mois+"/"+année+" n'est pas du tout valide");
				}
			}
			else if(mois==2) {
				if((année%4==0 && année%100!=0) || (année%400==0)) {
					if((jj>=1 && jj<=29)) {
						System.out.println(jj+"/"+mois+"/"+année+" est bien valide");
					}
					else {
						System.out.println(jj+"/"+mois+"/"+année+" n'est pas du tout valide");
					} 
				}
				else if((jj>=1 && jj<=28)) {
					System.out.println(jj+"/"+mois+"/"+année+" est bien valide");
				}
				else {
					System.out.println(jj+"/"+mois+"/"+année+" n'est pas du tout valide");
				}
			}
			else {
				if((jj>=1 && jj<=30)) {
					System.out.println(jj+"/"+mois+"/"+année+" est bien valide");
				}
				else {
					System.out.println(jj+"/"+mois+"/"+année+" n'est pas du tout valide");
				}
			}
		}
		else {
			System.out.println(jj+"/"+mois+"/"+année+" n'est pas du tout valide");
		}
	}

}
