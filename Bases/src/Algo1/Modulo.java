package Algo1;
import java.util.Scanner;
public class Modulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule le modulo par soustraction");
		System.out.println("Entrer la dividente");
		int dividente = clavier.nextInt();
		System.out.println("Entrer le diviseur");
		int diviseur = clavier.nextInt();
		int quotient = Modulo(dividente,diviseur);

		System.out.println("Le reste de la division est "+quotient);

	}

	public static int Modulo(int dividente, int diviseur) {
		int quotient;
		do {
			quotient = dividente - diviseur;
			dividente = quotient;
		}while(dividente >= diviseur);
		return quotient;
	}

}
