package Algo1;
import java.util.Scanner;
public class RechercheOccurence12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme recheche un élément dans un tableau et affiche Oui ou Non");
		int tab[] = {12,15,13,10,8,9,13,14};
		System.out.println("Entrer l'élément que vous cherchez:");
		int recherche = clavier.nextInt();
		boolean test =false;
		for (int i = 0; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
			if(recherche==tab[i]) {
				test = true;
			}
		}
		System.out.println();
		if (test) {
			System.out.println("********* OUI ******** Ce nombre "+recherche+" existe dans le tableau");
		} else {
			System.out.println("********* NON ******** Ce nombre "+recherche+" n'existe pas dans le tableau");
		}

	}

}
