package Algo1;
import java.util.Scanner;
public class OccurrenceTab {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme recherche une occurrence dans le tableau et donne sa position");
		int tab[]= {12,15,17,38,32,27,11,19,23,40,92,56,77,85,66,48,97,78,8};
		System.out.println("Entrez l'occurrence que vous cherchez");
		int nbre = clavier.nextInt();
		int pos = 0;
		boolean test =false;
		for (int i = 0; i < tab.length; i++) {
			if(tab[i]==nbre) {
				pos=i;
				test = true;
			}
		}
		if(test) {
			System.out.println("L'occurence "+nbre+" existe et se trouve à la position "+(pos+1));
			System.out.println("Voici les éléments du tableau: ");
			for (int i = 0; i < tab.length; i++) {
				System.out.print(tab[i]+" ");
			}
		}
		else {
			System.out.println("Cette occurrence "+nbre+" n'existe pas dans le tableau");
			System.out.println("Voici les éléments du tableau: ");
			for (int i = 0; i < tab.length; i++) {
				System.out.print(tab[i]+" ");
			}
		}

	}

}
