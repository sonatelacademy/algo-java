package Algo1;
import java.util.Scanner;

import Algo.CatégorieAge;
public class AfficheEnvers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme demande un nombre et l'affiche à l'envers. exemple 234 donne 432");;
		String nombre="";
		boolean test = true;
		do {
			System.out.println("Entrer un nombre");
			nombre = clavier.next();	
			test = true;
			for (int i = 0; i < nombre.length(); i++) {
				if(!(Character.isDigit(nombre.charAt(i)))) {
						test = false;
				}
			}
		}while(!test);
		
		int nbre = Integer.valueOf(nombre);
		System.out.print("Son nombre à l'envers est: ");
		int reste;
		do {
			reste=nbre%10;
			System.out.print(reste);
			nbre =nbre/10;
		} while (nbre!=0);
	}

}
