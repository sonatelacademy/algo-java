package Algo1;

public class SommeValeurTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Bienvenue! Ce programme calcule la somme des valeurs d'un tableau préalablement saisi.");
		int tab[]= {12,15,17,38,32,27,11,19,23,40,92,56,77,85,66,48,97,78,8};
		int som =0;
		System.out.println("Voici le tableau préalablement saisi: ");
		for (int i = 0; i < tab.length; i++) {
			som+=tab[i];
			System.out.print(tab[i]+" ");
		}
		System.out.println();
		System.out.println("La somme des valeurs de ce tableau est "+som);

	}

}
