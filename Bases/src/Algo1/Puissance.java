package Algo1;
import java.util.Scanner;
public class Puissance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Bienvenue! Ce programme calcule X puissance de Y par Addition et Multiplication");
		System.out.println("Voici le Format X puissance Y");
		System.out.println("Entrez la valeur de X");
		int X = clavier.nextInt();
		System.out.println("Entrez la valeur de Y");
		int Y = clavier.nextInt();
		int puis=1;
		for (int i = 1; i <= Y; i++) {
			puis*= X;
		}
		System.out.println(X+" exp "+Y+" = "+puis);
		
	}

}
