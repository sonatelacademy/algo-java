package Algo1;

public class EcartTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int tab[]= {12,15,17,38,32,27,11,19,23,40,92,56,77,85,66,48,97,78,8};
		System.out.println("Bienvenue! Ce programme cherche le plus grand écart entre deux éléments d'un tableau");
		System.out.println("Voici le tableau prédéfini");
		int ecart1, ecart2;
		ecart2 = tab[1]-tab[0];
		int x = tab[1];
		int y = tab[0];
		for (int i = 2; i < tab.length; i++) {
			System.out.print(tab[i]+" ");
			ecart1=tab[i]-tab[i-1];
			if (Math.abs(ecart1)>=Math.abs(ecart2)) {
				ecart2=ecart1;
				x = tab[i];
				y = tab[i-1];
			}
		}
		System.out.println();
		System.out.println("Le plus grand écart est "+ecart2+" et est entre "+y+" et "+x);
	}

}
