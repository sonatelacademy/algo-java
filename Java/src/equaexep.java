/* Importation des bibliotheques */
import java.util.*;
import java.lang.Math;

/* Definition de la classe Equation1 */
public class Equation1{

  /* Defintion de la fonction main */
  public static void main(String[] args) {

    /* Teste du nombre d'argumnts en entree */
    if(args.length == 3){

      /* Declaration et affectation des coefficients de l'equation */
      float a, b, c;
      try{
        a = Float.parseFloat(args[0]);
        b = Float.parseFloat(args[1]);
        c = Float.parseFloat(args[2]);

        /* Cas a = 0 */
        if(a == 0){

          /* sous cas b = 0 */
          if(b == 0){
            System.out.println("L'equation "+c+" = 0 : n'admet pas de solution");
          }

          /* sous cas b != 0 */
          else{
            if(c < 0){
              System.out.println("L'equation "+b+"x "+c+" = 0 : admet une solution s = "+(-c/b));
            }
            else{
              if(c > 0){
                System.out.println("L'equation "+b+"x + "+c+" = 0 : admet une solution s = "+(-c/b));
              }
              else{
                System.out.println("L'equation "+b+"x = 0 : admet une solution s = "+(-c/b));
              }
            }
          }
        }

        /* Cas a != 0 */
        else{
          float det = b*b - 4*a*c;

          /* Cas determinant < 0 */
          if(det < 0){
            System.out.println("L'equation n'admet pas de solutions dans R.");
          }
          else{

            /* Cas determinant = 0 */
            if(det == 0){
              System.out.println("L'equation admet une solution s = "+(-b/(2*a)));
            }

            /* Cas determinant > 0 */
            else{
              double x1 = (-b - Math.sqrt(det))/(2*a);
              double x2 = (-b + Math.sqrt(det)/(2*a));
              System.out.println("L'equation admet deux solutions s1 = "+x1+" et s2 = "+x2);
            }
          }
        }
      }
      catch (NumberFormatException e) {
        System.out.println("Impossible de resoudre l'equation car les coefficient ne sont pas des nombres.");
      }
    }

    /* Nombre d'arguments incorrecte */
    else{
      System.out.println("Impossible de resoudre une equation. Le nombre d'arguments est incorrecte.");
    }
  }

}