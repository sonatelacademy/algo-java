/* Importation des bibliotheques */
import java.util.*;

/* Definition de la classe Calculatrice1 */
public class Calculatrice1 {

  /* Defintion de la fonction main */
  public static void main(String[] args) {

    /* Teste du nombre d'argumnts en entree */
    if(args.length == 3){

      /* Declaration et affectation des operandes de l'operation */
      float a, b;
      try{
        a = Float.parseFloat(args[0]);
        b = Float.parseFloat(args[2]);
        switch(args[1]){
          case "+":
            System.out.println("La somme de "+a+" et "+b+" donne "+(a+b));
            break;
          case "-":
            System.out.println("La difference entre "+a+" et "+b+" donne "+(a-b));
            break;
          case "x":
            System.out.println("Le produit de "+a+" et "+b+" donne "+(a*b));
            break;
          case "/":
            if(b == 0){
              System.out.println("Impossible de diviser "+a+" par "+b);
            }
            else{
              System.out.println("La division de "+a+" par "+b+" donne "+(a/b));
            }
            break;
          default:
            System.out.println("L'operateur saisi est incorrect");
            break;
        }
      }
      catch (NumberFormatException e) {
        System.out.println("Impossible d'effectuer l'operation");
      }
    }

    /* Nombre d'arguments incorrecte */
    else{
      System.out.println("Le nombre d'arguments est incorrecte.");
    }
  }

}